intel_version="2021.10.0"
toolchain_compiler_package="intel-oneapi-compilers-classic@${intel_version}"
export TOOLCHAIN_COMPILER="intel@${intel_version}"
unset intel_version

toolchain_gcc_package="gcc@12.3.0"
export TOOLCHAIN_GCC=$toolchain_gcc_package
