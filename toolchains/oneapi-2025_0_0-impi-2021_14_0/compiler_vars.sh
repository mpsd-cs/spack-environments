intel_version="2025.0.0"
toolchain_compiler_package="intel-oneapi-compilers@${intel_version}"
export TOOLCHAIN_COMPILER="oneapi@${intel_version}"
unset intel_version

toolchain_gcc_package="gcc@14.2.0"
export TOOLCHAIN_GCC=$toolchain_gcc_package
