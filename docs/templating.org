#+TITEL: Jinja templating in package sets and spack_overlay
A number of settings/options in the spack_overlay and the environments'
spack.yaml files use Jinja templating. This document summarises the reasons and
documents required/available keys.

* Toolchains

A toolchain is a combination of one or multiple compilers that are installed
globally and a spack environment in which all packages are installed with these
compilers.

** toolchain_name/compilers.yaml
defines the required compiler(s) for that toolchain (custom format)
*** required
#+begin_src yaml
  toolchain_compiler:
    spack_package: "name of compiler package@version"
    spack_compiler: "name of compiler@version"
#+end_src

The split into package and compiler is required because the intel
package/compiler have different names. For consistency both keys are required
even if package and compiler have the same name.

*** optional
Intel toolchains also require a GCC. Therefore an additional ~fallback_compiler~
entry can optionally be defined.
#+begin_src yaml
  fallback_compiler:
    spack_package: "name of compiler package@version"
    spack_compiler: "name of compiler@version"
#+end_src

** toolchain_name/spack.yaml
defines the spack environment (standard spack format)

jinja template syntax is used to request the correct compiler(s); available
variables (based on compilers.yaml) are:
|--------------------+----------------------------------------------------------------|
| variable           | value                                                          |
|--------------------+----------------------------------------------------------------|
| toolchain_compiler | value of toolchain_compiler.spack_compiler                     |
| fallback_compiler  | value of fallback_compiler.spack_compiler (fails if undefined) |

* Global package sets

TODO

* spack_overlay

Several MPSD specific settings/paths are defined in the Spack configuration
files in ~spack_overlay~.

Available variables are defined in (or determined by) the ~software-manager~:

|-----------------+--------------------------------------------------------------------------------------------------------------------------------|
| variable        | value                                                                                                                          |
|-----------------+--------------------------------------------------------------------------------------------------------------------------------|
| source_cache    | Path to global source cache, pre-defined in ~software-manager~, user can overwrite                                             |
| spack_root      | Path of spack directory, hard-coded to MPSD_SOFTWARE_ROOT/software-release/microarch/spack, MPSD_SOFTWARE_ROOT is user-defined |
| lmod_root       | Path of spack directory, hard-coded to MPSD_SOFTWARE_ROOT/software-release/micorarch/lmod, MPSD_SOFTWARE_ROOT is user-defined  |
| system_compiler | Spec of the system compiler (typically gcc@version), auto-detected by ~software-manager~, user can overwrite                   |
