* About
This repository is used to define software provided on the [[https://computational-science.mpsd.mpg.de/docs/mpsd-hpc.html][MPSD HPC system]]. We use [[https://spack.io/][Spack]] to provide
all software, users can load the software using ~lmod~.

Installation of the software is done with the [[https://gitlab.gwdg.de/mpsd-cs/mpsd-software-manager][mpsd software manager-2]].

The repository is based on a fork of Spack that can be found at
[[https://github.com/mpsd-computational-science/spack]]. This fork contains branches that match the
~release~ branches in this repository. The fork is used to backport newer package versions to the
latest Spack release and to allow for MPSD specific modifications.

Parts of the software provided via spack replicate [[https://docs.easybuild.io/common-toolchains/#common_toolchains_overview][EasyBuild toolchains]], which are required for the
Octopus buildbot.

We make regular releases of the software stack on the HPC system (naming convention: two-digit
year + one letter, roughly based on when work on a particular release started). After a release the
software stack will not be modified anymore.

Users can request additional software, which we will generally include in the next release. Early
access to upcoming releases will be provided to individual users to allow them test the new software
prior to the official release.

* MPSD software releases

| release | Spack | toolchains                                                                                                                                                                                  | release date |
|---------+-------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------|
| [[https://gitlab.gwdg.de/mpsd-cs/spack-environments/-/tree/releases/dev-23a][dev-23a]] | [[https://github.com/mpsd-computational-science/spack/tree/mpsd/v0.19_dev-23a][v0.19]] | foss2021a, foss2022a (limited set of packages)                                                                                                                                              | March 2023   |
| [[https://gitlab.gwdg.de/mpsd-cs/spack-environments/-/tree/releases/23b][23b]]     | [[https://github.com/mpsd-computational-science/spack/tree/mpsd/v0.19_23b][v0.19]] | foss2021a, foss2022a                                                                                                                                                                        | October 2023 |
| [[https://gitlab.gwdg.de/mpsd-cs/spack-environments/-/tree/releases/24a][24a]]     | [[https://github.com/mpsd-computational-science/spack/tree/mpsd/v0.21_24a][v0.21]] | foss2022a, foss2022b, foss2023a, foss2023b, foss2022a-cuda, intel2022a, intel2022b, intel2023a                                                                                              | June 2024    |
| [[https://gitlab.gwdg.de/mpsd-cs/spack-environments/-/tree/releases/25a][25a]]     | [[https://github.com/mpsd-computational-science/spack/tree/mpsd/v0.23_25a][v0.23]] | GCC 11.5 (OpenMPI 4.1.4), GCC 12.3 (OpenMPI 4.1.5), GCC 13.2 (OpenMPI 4.1.6), GCC 14.2 (OpenMPI 5.0.5), GCC 13.2 + CUDA 12.6.2 (OpenMPI 4.1.4), Intel Oneapi 2025.0.0 (Intel MPI 2021.14.0) | January 2025 |

- For releases dev-23a, 23b and 24a the names are based on [[https://docs.easybuild.io/common-toolchains/#common_toolchains_overview][EasyBuild toolchains]]
- All toolchains are provided in two flavours, without and with MPI

* Access to the software on the HPC system
For detailed documentation refer to the [[https://computational-science.mpsd.mpg.de/docs/mpsd-hpc.html][documentation of the MPSD HPC system]].
