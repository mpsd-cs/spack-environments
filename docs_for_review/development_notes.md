# Update Spack

The repository uses spack from our fork at https://github.com/mpsd-computational-science/spack. This fork has a set of custom branches of the form `mpsd/<spack_release>_<mpsd_software_release>`, e.g. `mpsd/v0.21_24a`.

These branches are based off of the spack release tags. In order to update to a new release, it is necessary, to first pull in the latest release tags. This cannot be done (as of Dec 2023) via the GitHub UI. Instead, we have to locally set up different remotes (e.g. `origin` as the default pointing to our fork and in addition `upstream` pointing to the official spack repository) and then use
```
git fetch --tags upstream
git push --tags  # origin is the default
```

Now, a new branch can be created starting from the latest (desired) release tag.

This branch name then has to be updated in `spack-setup.sh` (in this repository) in the variable `spack_branch`, e.g. `spack_branch='mpsd/v0.21_24a'`. This change should happen in a branch (in spack-environments) with the name `releases/<mpsd_software_release>`, e.g. `releases/24a` goes together with `mpsd/v0.21_24a`.

# Start a new release

If desired update to a new spack release as described above.

Create a new branch `releases/<mpsd_software_release>`, e.g. `releases/24a` in the `spack-environments` repository. Now, `mpsd-software-manager` can be used to install existing toolchains in the new release.

# Module organisation in Easybuild

Separation between toolchain (foss or intel) and 'Octopus' (a meta package to load all dependencies for octopus; different variants are available, e.g. 'min', 'full', and 'full-mpi')

## Foss

```
$ module load foss/2022b
$ module list

Currently Loaded Modules:
  1) GCCcore/12.2.0   5) numactl/2.0.16      9) hwloc/2.8.0      13) libfabric/1.16.1  17) OpenBLAS/0.3.21  21) ScaLAPACK/2.2.0-fb
  2) zlib/1.2.12      6) XZ/5.2.7           10) OpenSSL/1.1      14) PMIx/4.2.2        18) FlexiBLAS/3.2.1  22) foss/2022b
  3) binutils/2.39    7) libxml2/2.10.3     11) libevent/2.1.12  15) UCC/1.1.0         19) FFTW/3.3.10
  4) GCC/12.2.0       8) libpciaccess/0.17  12) UCX/1.13.1       16) OpenMPI/4.1.5     20) FFTW.MPI/3.3.10

```

After loading the toolchain, the following modules are available:

```
$ module avail

--------------------- /opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022b/modules/MPI/GCC/12.2.0/OpenMPI/4.1.5 ----------------------
   BerkeleyGW/1.2.0-serial        HDF5/1.14.0           Octopus/min-mpi             ScaLAPACK/2.2.0-fb    (L)
   BerkeleyGW/1.2.0        (D)    NFFT/3.2.4            Octopus/min                 futile/1.8.3-serial
   CGAL/4.14.3                    Octopus/debug-mpi     Octopus/warnings-mpi (D)    futile/1.8.3          (D)
   DFTB+/21.2-serial              Octopus/debug         PFFT/1.0.8-alpha            libvdwxc/0.4.0-serial
   DFTB+/21.2              (D)    Octopus/full-mpi      PNFFT/1.0.7-alpha           libvdwxc/0.4.0        (D)
   ELPA/2022.11.001               Octopus/full          PSolver/1.8.3-serial        netCDF-Fortran/4.6.0
   ETSF_IO/1.0.4                  Octopus/libxc6-mpi    PSolver/1.8.3        (D)    netCDF/4.9.0
   FFTW.MPI/3.3.10         (L)    Octopus/libxc6        ParMETIS/4.0.3

-------------------------- /opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022b/modules/Compiler/GCC/12.2.0 --------------------------
   BLIS/0.9.0      FFTW/3.3.10     (L)    GSL/2.7         (D)    OpenMPI/4.1.5        (L)    libpspio/0.2.4    libxc/6.1.0 (D)
   Boost/1.81.0    FlexiBLAS/3.2.1 (L)    OpenBLAS/0.3.21 (L)    SPARSKIT2/2021.06.01        libxc/5.2.3

------------------------ /opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022b/modules/Compiler/GCCcore/12.2.0 ------------------------
   Autoconf/2.71                       NASM/2.15.05                cairo/1.17.4                   libjpeg-turbo/2.1.4
   Automake/1.16.5                     NLopt/2.7.1                 double-conversion/3.2.1        libpciaccess/0.17   (L)
   Autotools/20220317                  NSPR/4.35                   expat/2.4.9                    libpng/1.6.38
   Bison/3.8.2                  (D)    NSS/3.85                    flex/2.6.4              (D)    libreadline/8.2
   Brotli/1.0.9                        Ninja/1.11.1                fontconfig/2.14.1              libtool/2.4.7
   CMake/3.24.3                        PCRE2/10.40                 freetype/2.12.1                libunwind/1.6.2
   DB/18.1.40                          PMIx/4.2.2         (L)      gettext/0.21.1          (D)    libxml2/2.10.3      (L)
   DBus/1.15.2                         Perl/5.36.0                 git/2.38.1-nodocs              libyaml/0.2.5
   Doxygen/1.9.5                       Python/2.7.18-bare          gperf/3.1                      lz4/1.9.4
   Eigen/3.4.0                         Python/3.10.8-bare          graphite2/1.3.14               make/4.3
   GLib/2.75.0                         Python/3.10.8      (D)      groff/1.22.4                   ncurses/6.3         (D)
   GMP/6.2.1                           Qt5/5.15.7                  gzip/1.12                      nodejs/18.12.1
   GObject-Introspection/1.74.0        Rust/1.65.0                 help2man/1.49.2                numactl/2.0.16      (L)
   GSL/2.7                             SQLite/3.39.4               hwloc/2.8.0             (L)    pixman/0.42.2
   HarfBuzz/5.3.1                      Szip/2.1.1                  intltool/0.51.0                pkg-config/0.29.2
   ICU/72.1                            Tcl/8.6.12                  libGLU/9.0.2                   pkgconf/1.9.3       (D)
   JasPer/4.0.0                        UCC/1.1.0          (L)      libarchive/3.6.1               re2c/3.0
   LLVM/15.0.5                         UCX/1.13.1         (L)      libdrm/2.4.114                 snappy/1.1.9
   M4/1.4.19                    (D)    UnZip/6.0                   libevent/2.1.12         (L)    util-linux/2.38.1
   METIS/5.1.0                         X11/20221110                libfabric/1.16.1        (L)    xorg-macros/1.19.3
   MPFR/4.2.0                          XZ/5.2.7           (L)      libffi/3.4.4                   zlib/1.2.12         (L,D)
   Mako/1.2.4                          binutils/2.39      (L,D)    libgd/2.3.3                    zstd/1.5.2
   Mesa/22.2.4                         bzip2/1.0.8                 libglvnd/1.6.0
   Meson/0.64.0                        cURL/7.86.0                 libiconv/1.17

--------------------------------- /opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022b/modules/Core ----------------------------------
   Bison/3.8.2           M4/1.4.19            flex/2.6.4            gompi/2022b      intel-compilers/2022.2.1    pkgconf/1.8.0
   GCC/12.2.0     (L)    OpenSSL/1.1   (L)    foss/2022b     (L)    iimpi/2022b      intel/2022b                 zlib/1.2.12
   GCCcore/12.2.0 (L)    binutils/2.39        gettext/0.21.1        imkl/2022.2.1    ncurses/6.3

```

Observations:

- gcc is split into two modules, GCC and GCCcore; most modules depend on GCCcore; spack does not have this split
- Autotools and cmake are compiled with the toolchain compiler
- All Octopus meta packages seem to depend on MPI, even if they (based on their name) are serial (i.e. non-MPI)
- Easybuild seems to use the term `serial` to indicate non-MPI builds

```
$ module load Octopus/min
$ module list

Currently Loaded Modules:
  1) GCCcore/12.2.0   7) libxml2/2.10.3     13) libfabric/1.16.1  19) FFTW/3.3.10         25) ncurses/6.3      31) libtool/2.4.7
  2) zlib/1.2.12      8) libpciaccess/0.17  14) PMIx/4.2.2        20) FFTW.MPI/3.3.10     26) libreadline/8.2  32) Autotools/20220317
  3) binutils/2.39    9) hwloc/2.8.0        15) UCC/1.1.0         21) ScaLAPACK/2.2.0-fb  27) DB/18.1.40       33) libxc/5.2.3
  4) GCC/12.2.0      10) OpenSSL/1.1        16) OpenMPI/4.1.5     22) foss/2022b          28) Perl/5.36.0      34) GSL/2.7
  5) numactl/2.0.16  11) libevent/2.1.12    17) OpenBLAS/0.3.21   23) M4/1.4.19           29) Autoconf/2.71    35) Octopus/min
  6) XZ/5.2.7        12) UCX/1.13.1         18) FlexiBLAS/3.2.1   24) expat/2.4.9         30) Automake/1.16.5

```

The Octopus/min meta package sets environment variables `CC = gcc` *and* `MPICC = mpicc` etc.

## Intel

```
$ module load intel/2022b
$ module list

Currently Loaded Modules:
  1) GCCcore/12.2.0   3) binutils/2.39              5) numactl/2.0.16   7) impi/2021.7.1   9) imkl-FFTW/2022.2.1
  2) zlib/1.2.12      4) intel-compilers/2022.2.1   6) UCX/1.13.1       8) imkl/2022.2.1  10) intel/2022b

```

After loading the toolchain, the following modules are available

```
$ module avail

------------------- /opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022b/modules/MPI/intel/2022.2.1/impi/2021.7.1 --------------------
   BerkeleyGW/1.2.0-serial        ETSF_IO/1.0.4       Octopus/full         (D)    ParMETIS/4.0.3               libvdwxc/0.4.0       (D)
   BerkeleyGW/1.2.0        (D)    FFTW/3.3.10         PFFT/1.0.8-alpha            futile/1.8.3-serial          netCDF-Fortran/4.6.0
   DFTB+/21.2-serial              HDF5/1.14.0         PNFFT/1.0.7-alpha           futile/1.8.3          (D)    netCDF/4.9.0
   DFTB+/21.2              (D)    NFFT/3.2.4          PSolver/1.8.3-serial        imkl-FFTW/2022.2.1    (L)
   ELPA/2022.11.001               Octopus/full-mpi    PSolver/1.8.3        (D)    libvdwxc/0.4.0-serial

------------------------ /opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022b/modules/Compiler/intel/2022.2.1 ------------------------
   SPARSKIT2/2021.06.01    dftd3-lib/0.10    impi/2021.7.1 (L)    libxc/5.2.3

------------------------ /opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022b/modules/Compiler/GCCcore/12.2.0 ------------------------
   Autoconf/2.71                       NASM/2.15.05                cairo/1.17.4                   libjpeg-turbo/2.1.4
   Automake/1.16.5                     NLopt/2.7.1                 double-conversion/3.2.1        libpciaccess/0.17
   Autotools/20220317                  NSPR/4.35                   expat/2.4.9                    libpng/1.6.38
   Bison/3.8.2                  (D)    NSS/3.85                    flex/2.6.4              (D)    libreadline/8.2
   Brotli/1.0.9                        Ninja/1.11.1                fontconfig/2.14.1              libtool/2.4.7
   CMake/3.24.3                        PCRE2/10.40                 freetype/2.12.1                libunwind/1.6.2
   DB/18.1.40                          PMIx/4.2.2                  gettext/0.21.1          (D)    libxml2/2.10.3
   DBus/1.15.2                         Perl/5.36.0                 git/2.38.1-nodocs              libyaml/0.2.5
   Doxygen/1.9.5                       Python/2.7.18-bare          gperf/3.1                      lz4/1.9.4
   Eigen/3.4.0                         Python/3.10.8-bare          graphite2/1.3.14               make/4.3
   GLib/2.75.0                         Python/3.10.8      (D)      groff/1.22.4                   ncurses/6.3         (D)
   GMP/6.2.1                           Qt5/5.15.7                  gzip/1.12                      nodejs/18.12.1
   GObject-Introspection/1.74.0        Rust/1.65.0                 help2man/1.49.2                numactl/2.0.16      (L)
   GSL/2.7                             SQLite/3.39.4               hwloc/2.8.0                    pixman/0.42.2
   HarfBuzz/5.3.1                      Szip/2.1.1                  intltool/0.51.0                pkg-config/0.29.2
   ICU/72.1                            Tcl/8.6.12                  libGLU/9.0.2                   pkgconf/1.9.3       (D)
   JasPer/4.0.0                        UCC/1.1.0                   libarchive/3.6.1               re2c/3.0
   LLVM/15.0.5                         UCX/1.13.1         (L)      libdrm/2.4.114                 snappy/1.1.9
   M4/1.4.19                    (D)    UnZip/6.0                   libevent/2.1.12                util-linux/2.38.1
   METIS/5.1.0                         X11/20221110                libfabric/1.16.1               xorg-macros/1.19.3
   MPFR/4.2.0                          XZ/5.2.7                    libffi/3.4.4                   zlib/1.2.12         (L,D)
   Mako/1.2.4                          binutils/2.39      (L,D)    libgd/2.3.3                    zstd/1.5.2
   Mesa/22.2.4                         bzip2/1.0.8                 libglvnd/1.6.0
   Meson/0.64.0                        cURL/7.86.0                 libiconv/1.17

--------------------------------- /opt_buildbot/linux-debian11/sandybridge/EasyBuild/2022b/modules/Core ----------------------------------
   Bison/3.8.2           M4/1.4.19        flex/2.6.4        gompi/2022b          intel-compilers/2022.2.1 (L)    pkgconf/1.8.0
   GCC/12.2.0            OpenSSL/1.1      foss/2022b        iimpi/2022b          intel/2022b              (L)    zlib/1.2.12
   GCCcore/12.2.0 (L)    binutils/2.39    gettext/0.21.1    imkl/2022.2.1 (L)    ncurses/6.3

```

Observations:

- The intel toolchain does contain GCCcore
- **many** of the more basic packages are compiled with gcc
- Intel based FFTW contains serial, openMP and MPI variants; foss based FFTW only contains serial and openMP, and the additional FFTW.MPI provides the mpi variant

```
$ module load Octopus/full
$ module list

Currently Loaded Modules:
  1) GCCcore/12.2.0            13) ncurses/6.3         25) cURL/7.86.0           37) NLopt/2.7.1            49) libyaml/0.2.5
  2) zlib/1.2.12               14) libreadline/8.2     26) gzip/1.12             38) libpng/1.6.38          50) futile/1.8.3-serial
  3) binutils/2.39             15) OpenSSL/1.1         27) XZ/5.2.7              39) Brotli/1.0.9           51) PSolver/1.8.3-serial
  4) intel-compilers/2022.2.1  16) DB/18.1.40          28) lz4/1.9.4             40) freetype/2.12.1        52) BerkeleyGW/1.2.0-serial
  5) numactl/2.0.16            17) Perl/5.36.0         29) zstd/1.5.2            41) util-linux/2.38.1      53) Tcl/8.6.12
  6) UCX/1.13.1                18) Autoconf/2.71       30) bzip2/1.0.8           42) fontconfig/2.14.1      54) SQLite/3.39.4
  7) impi/2021.7.1             19) Automake/1.16.5     31) libxml2/2.10.3        43) NASM/2.15.05           55) GMP/6.2.1
  8) imkl/2022.2.1             20) libtool/2.4.7       32) netCDF/4.9.0          44) libjpeg-turbo/2.1.4    56) libffi/3.4.4
  9) imkl-FFTW/2022.2.1        21) Autotools/20220317  33) netCDF-Fortran/4.6.0  45) libgd/2.3.3            57) Python/3.10.8
 10) intel/2022b               22) libxc/5.2.3         34) ETSF_IO/1.0.4         46) FFTW/3.3.10            58) DFTB+/21.2-serial
 11) M4/1.4.19                 23) Szip/2.1.1          35) GSL/2.7               47) libvdwxc/0.4.0-serial  59) Octopus/full
 12) expat/2.4.9               24) HDF5/1.14.0         36) SPARSKIT2/2021.06.01  48) NFFT/3.2.4

```

For comparison, **foss Octopus/full**:

```
module load foss Octopus/full
$ module list

Currently Loaded Modules:
  1) GCCcore/12.2.0     20) FFTW.MPI/3.3.10     39) zstd/1.5.2             58) libyaml/0.2.5            77) libGLU/9.0.2
  2) zlib/1.2.12        21) ScaLAPACK/2.2.0-fb  40) bzip2/1.0.8            59) futile/1.8.3-serial      78) double-conversion/3.2.1
  3) binutils/2.39      22) foss/2022b          41) netCDF/4.9.0           60) PSolver/1.8.3-serial     79) gettext/0.21.1
  4) GCC/12.2.0         23) M4/1.4.19           42) netCDF-Fortran/4.6.0   61) BerkeleyGW/1.2.0-serial  80) PCRE2/10.40
  5) numactl/2.0.16     24) expat/2.4.9         43) ETSF_IO/1.0.4          62) Tcl/8.6.12               81) GLib/2.75.0
  6) XZ/5.2.7           25) ncurses/6.3         44) GSL/2.7                63) SQLite/3.39.4            82) pixman/0.42.2
  7) libxml2/2.10.3     26) libreadline/8.2     45) SPARSKIT2/2021.06.01   64) GMP/6.2.1                83) cairo/1.17.4
  8) libpciaccess/0.17  27) DB/18.1.40          46) NLopt/2.7.1            65) libffi/3.4.4             84) HarfBuzz/5.3.1
  9) hwloc/2.8.0        28) Perl/5.36.0         47) libpng/1.6.38          66) Python/3.10.8            85) graphite2/1.3.14
 10) OpenSSL/1.1        29) Autoconf/2.71       48) Brotli/1.0.9           67) ICU/72.1                 86) DBus/1.15.2
 11) libevent/2.1.12    30) Automake/1.16.5     49) freetype/2.12.1        68) Boost/1.81.0             87) NSPR/4.35
 12) UCX/1.13.1         31) libtool/2.4.7       50) util-linux/2.38.1      69) MPFR/4.2.0               88) NSS/3.85
 13) libfabric/1.16.1   32) Autotools/20220317  51) fontconfig/2.14.1      70) xorg-macros/1.19.3       89) snappy/1.1.9
 14) PMIx/4.2.2         33) libxc/5.2.3         52) NASM/2.15.05           71) X11/20221110             90) JasPer/4.0.0
 15) UCC/1.1.0          34) Szip/2.1.1          53) libjpeg-turbo/2.1.4    72) libdrm/2.4.114           91) nodejs/18.12.1
 16) OpenMPI/4.1.5      35) HDF5/1.14.0         54) libgd/2.3.3            73) libglvnd/1.6.0           92) Qt5/5.15.7
 17) OpenBLAS/0.3.21    36) cURL/7.86.0         55) libpspio/0.2.4         74) libunwind/1.6.2          93) CGAL/4.14.3
 18) FlexiBLAS/3.2.1    37) gzip/1.12           56) libvdwxc/0.4.0-serial  75) LLVM/15.0.5              94) DFTB+/21.2-serial
 19) FFTW/3.3.10        38) lz4/1.9.4           57) NFFT/3.2.4             76) Mesa/22.2.4              95) Octopus/full

```


